# pfwb_thesaurus - thesaurus system
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models


class Term(models.Model):
    class Meta:
        ordering = ['term']

    term = models.CharField(max_length=300)
    tabellio_id = models.CharField(max_length=10)

    broader = models.ManyToManyField('self', symmetrical=False,
            related_name='narrower')
    related = models.ManyToManyField('self', symmetrical=True)

    historical_note = models.TextField(blank=True)
    scope_note = models.TextField(blank=True)

    creation_timestamp = models.DateTimeField(auto_now_add=True)
    last_update_timestamp = models.DateTimeField(auto_now=True)


class AlternativeTerm(models.Model):
    primary_term = models.ForeignKey(Term)
    term = models.CharField(max_length=300)

    class Meta:
        ordering = ['term']
