# pfwb_thesaurus - thesaurus system
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.views.generic import DetailView, ListView

from haystack.query import SearchQuerySet


from .thesaurus.models import Term


class HomeView(ListView):
    model = Term

    def get_queryset(self, qs=None):
        return Term.objects.filter(term__in=settings.HOMEPAGE_START_TERMS)

home = HomeView.as_view()


class PlonePopupView(HomeView):
    template_name = 'thesaurus/plone_popup.html'

plone_popup = PlonePopupView.as_view()


class TermView(DetailView):
    model = Term

term = TermView.as_view()


def term_search_json(request):
    query = request.GET.get('q')
    searchqueryset = SearchQuerySet()
    sqs = searchqueryset.autocomplete(text_auto=query)
    sqs.load_all()

    result = []
    for item in sqs[:15]:
        result.append({'title': item.term, 'url': reverse('term', kwargs={'pk': item.pk}), 'id': item.pk})

    response = HttpResponse(content_type='application/json')
    json.dump({'data': result}, response, indent=2)
    return response
