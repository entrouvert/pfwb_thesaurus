# pfwb_thesaurus - thesaurus system
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from haystack import indexes

from .models import Term


class TermIndex(indexes.SearchIndex, indexes.Indexable):
    term = indexes.CharField(model_attr='term', boost=3)
    text = indexes.CharField(document=True)
    text_auto = indexes.EdgeNgramField()

    def get_model(self):
        return Term

    def prepare_text(self, obj):
        words = [obj.term]
        words.extend([x.term for x in obj.alternativeterm_set.all()])
        return ' '.join(words)

    def prepare_text_auto(self, obj):
        return self.prepare_text(obj)
