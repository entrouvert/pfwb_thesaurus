from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^$', 'pfwb_thesaurus.views.home', name='home'),
    url(r'^thesaurus/plone-popup/$', 'pfwb_thesaurus.views.plone_popup'),
    url(r'^thesaurus/term/search/json/$', 'pfwb_thesaurus.views.term_search_json', name='term-search-json'),
    url(r'^thesaurus/term/(?P<pk>\w+)$', 'pfwb_thesaurus.views.term', name='term'),

    url(r'^admin/', include(admin.site.urls)),
)
