# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AlternativeTerm',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('term', models.CharField(max_length=300)),
            ],
            options={
                'ordering': ['term'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Term',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('term', models.CharField(max_length=300)),
                ('tabellio_id', models.CharField(max_length=10)),
                ('historical_note', models.TextField(blank=True)),
                ('scope_note', models.TextField(blank=True)),
                ('creation_timestamp', models.DateTimeField(auto_now_add=True)),
                ('last_update_timestamp', models.DateTimeField(auto_now=True)),
                ('broader', models.ManyToManyField(related_name='narrower', to='thesaurus.Term')),
                ('related', models.ManyToManyField(related_name='related_rel_+', to='thesaurus.Term')),
            ],
            options={
                'ordering': ['term'],
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='alternativeterm',
            name='primary_term',
            field=models.ForeignKey(to='thesaurus.Term'),
            preserve_default=True,
        ),
    ]
