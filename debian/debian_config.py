# This file is sourced by "execfile" from pfwb-thesaurus.settings

import os

PROJECT_NAME = 'pfwb-thesaurus'

# SAML2 authentication
INSTALLED_APPS += ('mellon',)

#
# hobotization (multitenant)
#
execfile('/usr/lib/hobo/debian_config_common.py')

#
# local settings
#
execfile(os.path.join(ETC_DIR, 'settings.py'))
